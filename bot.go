package main

import (
    "github.com/jinzhu/gorm"
    _ "github.com/jinzhu/gorm/dialects/sqlite"
    "github.com/go-telegram-bot-api/telegram-bot-api"
    "log"
    "os"
)

// Tables
type Bridges struct {
    gorm.Model
    id string `gorm:"primary_key"`
    tg string // Telegram Chat ID
    mx string // Matrix Chat ID
}

func main() {
    db, err := gorm.Open("sqlite3", "tema.db")
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()
    
    // Migrate the schema
    db.AutoMigrate(&Bridges{})
    
    bot, err := tgbotapi.NewBotAPI(os.Getenv("TEMA_TG_BOT_TOKEN"))
    if err != nil {
        log.Panic(err)
    }

    bot.Debug = true

    log.Printf("Authorized on account %s", bot.Self.UserName)

    u := tgbotapi.NewUpdate(0)
    u.Timeout = 60

    updates, err := bot.GetUpdatesChan(u)

    for update := range updates {
        if update.Message == nil { // ignore any non-Message Updates
            continue
        }
        
        chat_id := update.Message.Chat.ID
        chat_type := update.Message.Chat.Type
        msg_text := update.Message.Text
        msg_username := update.Message.From.UserName
        
        if chat_type == "group" || chat_type == "supergroup" {
            
        }

        msg := tgbotapi.NewMessage(chat_id, msg_username + ": " + msg_text)
        msg.ReplyToMessageID = update.Message.MessageID

        bot.Send(msg)
    }
}
