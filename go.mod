module gitlab.com/subins2000/tema

go 1.13

require (
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/jinzhu/gorm v1.9.11 // indirect
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
